import requests


def test_invocations():
    path_csv = "data_for_model_train_example.csv"
    url = "http://147.45.246.100:8003/invocations"
    files = {"file": open(path_csv, "rb")}

    response = requests.post(url, files=files)

    expected = {
        "predictions": {
            "indexes": [
                0,
                1,
                2,
                3,
                4
            ],
            "predictions": [
                0,
                1,
                1,
                0,
                1
            ],
            "probabilities": [
                0.11804,
                0.55605,
                0.99392,
                0.10148,
                0.98127
            ]
        }
    }

    assert response.status_code == 200
    assert response.json() == expected
