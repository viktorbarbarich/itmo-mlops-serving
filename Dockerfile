FROM python:3.10

WORKDIR /code

RUN pip install --upgrade pip

COPY src/inference.py /code/app/inference.py
COPY ./.env /code/app/.env
COPY ./requirements.txt /code/app/requirements.txt


RUN pip install -r /code/app/requirements.txt


CMD ["gunicorn", "app.inference:app", "--workers", "1", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:80"]
