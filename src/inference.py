import os

import mlflow
import numpy as np
import pandas as pd
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException
from loguru import logger
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import BaseModel


class Predictions(BaseModel):
    indexes: list[int]
    predictions: list[int]
    probabilities: list[float]


class PredictionResult(BaseModel):
    predictions: Predictions


load_dotenv()

app = FastAPI()

Instrumentator().instrument(app).expose(app)

MODEL_PATH = os.getenv("MODEL_PATH")


class Model:
    def __init__(
            self,
            logged_model: str = MODEL_PATH
    ):
        """
        To initialize the model
        :param logged_model:
        """
        self.model = mlflow.catboost.load_model(logged_model)

    def predict(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        To predict the output
        :param data:
        :return:
        """
        predictions = self.model.predict(data)
        return predictions

    def predict_proba(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        To predict the output of 1st class
        :param data:
        :return:
        """
        probabilities = self.model.predict_proba(data)[:, 1]
        return probabilities


model = Model()


# Create the POST endpoint with path '/invocations'
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)) -> PredictionResult:
    logger.debug(f"Received file: {file.filename}")

    if file.filename.endswith(".csv"):

        data = pd.read_csv(file.file)
        logger.debug(f"Data loaded: {data.shape}")

        predictions = model.predict(data)
        probabilities = np.round(model.predict_proba(data), 5)
        logger.debug(f"Predictions: {predictions.shape}")

        return PredictionResult(
            predictions=Predictions(
                indexes=data.index.tolist(),
                predictions=predictions.tolist(),
                probabilities=probabilities.tolist()
            )
        )

    else:
        # Raise a HTTP 400 Exception, indicating Bad Request
        # (you can learn more about HTTP response status codes here)
        raise HTTPException(
            status_code=400,
            detail="Invalid file format. Only CSV Files accepted."
        )


if (
        os.getenv("AWS_ACCESS_KEY_ID") is None or
        os.getenv("AWS_SECRET_ACCESS_KEY") is None
):
    exit(1)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run("inference:app", host="localhost", port=8000, reload=True)
